using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using MyRecipe.Models;
using MyRecipe.Data;
using MyRecipe.Data.DTOs;
using MyRecipe.Data.Enums;

namespace MyRecipe.Controllers
{
    [Route("[controller]")]
    public class AuthController : Controller
    {
        private readonly ILogger<AuthController> _logger;
        private readonly MyRecipeDBContext _dbContext;


        public AuthController(ILogger<AuthController> logger, MyRecipeDBContext dBContext)
        {
            _logger = logger;
            _dbContext = dBContext;
        }

        [HttpGet()]
        public IActionResult Index(string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View("Index");
        }

        [HttpPost("Login")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(string username, string password, string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            User user = _dbContext.Users.Where(user => user.Username == username).Include(user => user.Role).FirstOrDefault();

            if (user == null || user.Password != password || user.Status != UserStatusEnum.Active) {
                TempData["Error"] = "Invalid credentials!";
                return View("Index");
            }

            List<Claim> claims = new List<Claim> {
                new Claim(ClaimTypes.NameIdentifier, user.Username),
                new Claim(ClaimTypes.Name, $"{user.FirstName} {user.LastName}"),
                new Claim(ClaimTypes.Role, user.Role.Name.ToString())
            };

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            await HttpContext.SignInAsync(claimsPrincipal);
            return Redirect(returnUrl ?? "/Recipe/List");

        }

        [HttpGet("Logout")]
        [Authorize]
        public async Task<IActionResult> LogoutPage()
        {
            await HttpContext.SignOutAsync();
            
            return Redirect("/Auth");
        }


        [HttpPost("Register")]
        [ValidateAntiForgeryToken]
        public  IActionResult Register(UserRegistrationDTO userRegistration)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", userRegistration);
            }

            bool emailExists = _dbContext.Users.Where(user => user.Email == userRegistration.Email).Count() > 0;
            if (emailExists)
            {
                TempData["ErrorEmail"] = "This email address is already registered.";
                return View("Index", userRegistration);
            }

            bool usernameExists = _dbContext.Users.Where(user => user.Username == userRegistration.Username).Count() > 0;
            if (usernameExists)
            {
                TempData["ErrorUsername"] = "This username is already taken.";
                return View("Index", userRegistration);
            }

            try {
                Role role = _dbContext.Roles.Where(role => role.Name == UserRoleEnum.Client).FirstOrDefault();

                if (role == null) {
                    throw new Exception("Cannot find role 'Client'");
                }
                
                userRegistration.RoleId = role.Id;
                userRegistration.Status = UserStatusEnum.Active;

                // TODO: Encrypt the password before save
                _dbContext.Users.Add(userRegistration);
                _dbContext.SaveChanges();
            } catch (Exception e) {
                TempData["Error"] = $"Error creating user: {e.Message}";
                return View("Index", userRegistration);
            }

            return View("Index");
        }


        [HttpGet("Denied")]
        [Authorize]
        public IActionResult DeniedPage()
        {
            return View("DeniedPage");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View("Error!");
        }
    }
}