﻿using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyRecipe.Models;
using MyRecipe.Data;
using MyRecipe.Data.DAOs;
using System.Security.Claims;
using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using MyRecipe.Data.DAOs.Shared;

namespace MyRecipe.Controllers
{
    [Authorize(Roles="Admin")]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _logger;
        private readonly MyRecipeDBContext _dbContext;
        private readonly UserDAO _users;
        private readonly RoleDAO _usersRoles;

        private readonly User _authenticatedUser;

        public UserController(IHttpContextAccessor httpContextAccessor, ILogger<UserController> logger, MyRecipeDBContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
            _users = new UserDAO(dbContext);
            _usersRoles = new RoleDAO(dbContext);

            
            String username = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            this._authenticatedUser = _dbContext.Users.Where(user => user.Username == username).FirstOrDefault();
        }

        [HttpGet()]
        public IActionResult Index(string search = null, int page = 1, int itemsPerPage = 16)
        {
            ViewData["search"] = search;
            PaginatedList<User> users = _users.GetFiltered(search, page, itemsPerPage, this._authenticatedUser);

            return View(users);
        }

        [HttpGet("{username}/Edit")]
        public IActionResult GetEditUserModal(string username)
        {
            User user = _users.GetByUsername(username);

            return PartialView("_EditUserModalPartial", user);
        }

        [HttpPost("{username}/Edit")]
        [ValidateAntiForgeryToken]
        public IActionResult EditUser(string username, User modifiedUser)
        {
            User user = _users.GetByUsername(username);
            Role newRole = _usersRoles.GetByName(modifiedUser.Role.Name);

            user.Role = newRole;
            user.Status = modifiedUser.Status;

            this._dbContext.Update(user);
            this._dbContext.SaveChanges();

            return RedirectToAction("Index");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route("Error")]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
