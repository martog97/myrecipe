﻿using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyRecipe.Models;
using MyRecipe.Data;
using MyRecipe.Data.DAOs;
using System.Security.Claims;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using MyRecipe.Data.DAOs.Shared;
using MyRecipe.Data.DTOs;
using Microsoft.AspNetCore.Hosting;
using System.IO;


namespace MyRecipe.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("[controller]")]
    public class CategoryController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ILogger<CategoryController> _logger;
        private readonly MyRecipeDBContext _dbContext;

        private readonly CategoryDAO _categories;
        private readonly RecipeDAO _recipes;


        private readonly User _authenticatedUser;

        public CategoryController(IHttpContextAccessor httpContextAccessor, IWebHostEnvironment webHostEnvironment, ILogger<CategoryController> logger, MyRecipeDBContext dbContext)
        {
            _webHostEnvironment = webHostEnvironment;
            _logger = logger;
            _dbContext = dbContext;
            _categories = new CategoryDAO(dbContext);
            _recipes = new RecipeDAO(dbContext);


            String username = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            this._authenticatedUser = _dbContext.Users.Where(user => user.Username == username).FirstOrDefault();
        }

        [HttpGet()]
        public IActionResult Index(string search = null, int page = 1)
        {
            ViewData["search"] = search;
            int itemsPerPage = 6;

            PaginatedList<Category> categories = _categories.GetFiltered(search, page, itemsPerPage);

            return View(categories);
        }

        [HttpPost()]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CreateEditCategoryDTO newCategory)
        {
            Category category = new Category();
            category.Name = newCategory.Name;

            string uploadDir = Path.Combine(_webHostEnvironment.WebRootPath, "img/Category");
            string fileName = Guid.NewGuid().ToString() + "_" + newCategory.Image.FileName;
            string filePath = Path.Combine(uploadDir, fileName);

            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                newCategory.Image.CopyTo(fileStream);
            }

            category.ImagePath = fileName;

            this._dbContext.Add(category);
            this._dbContext.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet("{categoryId}/Edit")]
        public IActionResult GetEditCategoryModal(int categoryId)
        {
            CreateEditCategoryDTO categoryDTO = new CreateEditCategoryDTO();
            Category category = _categories.GetById(categoryId);
            categoryDTO.Id = category.Id;
            categoryDTO.Name = category.Name;
            categoryDTO.ImagePath = category.ImagePath;

            return PartialView("_EditCategoryModalPartial", categoryDTO);
        }

        [HttpPost("{categoryId}/Edit")]
        [ValidateAntiForgeryToken]
        public IActionResult EditCategory(int categoryId, CreateEditCategoryDTO newCategory)
        {
            Category category = _categories.GetById(categoryId);
            category.Name = newCategory.Name;

            if (newCategory.Image != null)
            {
                string imagesDir = Path.Combine(_webHostEnvironment.WebRootPath, "img/Category");
                string oldImagePath = Path.Combine(imagesDir, category.ImagePath);

                if (System.IO.File.Exists(oldImagePath))
                {
                    System.IO.File.Delete(oldImagePath);
                }

                string newImageName = Guid.NewGuid().ToString() + "_" + newCategory.Image.FileName;
                string newImagePath = Path.Combine(imagesDir, newImageName);

                using (var fileStream = new FileStream(newImagePath, FileMode.Create))
                {
                    newCategory.Image.CopyTo(fileStream);
                }

                category.ImagePath = newImageName;
            }

            this._dbContext.Update(category);
            this._dbContext.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost("{categoryId}/Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteCategory(int categoryId)
        {
            Category category = _categories.GetById(categoryId);

            if (category == null)
            {
                return NotFound();
            }

            RecipesFilterDTO recipesFilter = new RecipesFilterDTO();
            recipesFilter.CategoryId = categoryId;
            PaginatedList<RecipeDTO> recipes = _recipes.GetFiltered(this._authenticatedUser, recipesFilter);

            if (recipes.Count > 0)
            {
                return Forbid();
            }

            string imagesDir = Path.Combine(_webHostEnvironment.WebRootPath, "img/Category");
            string imagePath = Path.Combine(imagesDir, category.ImagePath);

            if (System.IO.File.Exists(imagePath))
            {
                System.IO.File.Delete(imagePath);
            }

            this._dbContext.Categories.Remove(category);
            this._dbContext.SaveChanges();

            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route("Error")]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
