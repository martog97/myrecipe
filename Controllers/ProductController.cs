﻿using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyRecipe.Models;
using MyRecipe.Data;
using MyRecipe.Data.DAOs;
using System.Security.Claims;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using MyRecipe.Data.DAOs.Shared;
using MyRecipe.Data.DTOs;

namespace MyRecipe.Controllers
{
    [Authorize(Roles="Admin")]
    [Route("[controller]")]
    public class ProductController : Controller
    {
        private readonly ILogger<ProductController> _logger;
        private readonly MyRecipeDBContext _dbContext;

        private readonly ProductDAO _products;
        private readonly RecipeDAO _recipes;


        private readonly User _authenticatedUser;

        public ProductController(IHttpContextAccessor httpContextAccessor, ILogger<ProductController> logger, MyRecipeDBContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
            _products = new ProductDAO(dbContext);
            _recipes = new RecipeDAO(dbContext);
            
            String username = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            this._authenticatedUser = _dbContext.Users.Where(user => user.Username == username).FirstOrDefault();
        }

        [HttpGet()]
        public IActionResult Index(string search = null, int page = 1, int itemsPerPage = 16)
        {
            ViewData["search"] = search;

            PaginatedList<Product> products = _products.GetFiltered(search, page, itemsPerPage);

            return View(products);
        }

        [HttpPost()]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Product newProduct)
        {
            this._dbContext.Add(newProduct);
            this._dbContext.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet("{productId}/Edit")]
        public IActionResult GetEditProductModal(int productId)
        {
            Product product = _products.GetById(productId);

            return PartialView("_EditProductModalPartial", product);
        }

        [HttpPost("{productId}/Edit")]
        [ValidateAntiForgeryToken]
        public IActionResult EditProduct(int productId, Product newProduct)
        {
            Product product = _products.GetById(productId);
            product.Name = newProduct.Name;
            product.Note = newProduct.Note;

            this._dbContext.Update(product);
            this._dbContext.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost("{productId}/Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteProduct(int productId)
        {
            Product product = _products.GetById(productId);

            if (product == null) {
                return NotFound();
            }

            RecipesFilterDTO recipesFilter = new RecipesFilterDTO();
            recipesFilter.ProductIds = new List<int>() {productId};
            PaginatedList<RecipeDTO> recipes = _recipes.GetFiltered(this._authenticatedUser, recipesFilter);
            
            if (recipes.Count > 0) {
                return Forbid();
            }
            
            this._dbContext.Products.Remove(product);
            this._dbContext.SaveChanges();

            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route("Error")]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
