﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyRecipe.Models;
using MyRecipe.Data;
using MyRecipe.Data.DAOs;
using MyRecipe.Data.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Security.Claims;
using MyRecipe.Data.DAOs.Shared;
using System.Text.Json;

namespace MyRecipe.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class RecipeController : Controller
    {

        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ILogger<RecipeController> _logger;
        private readonly MyRecipeDBContext _dbContext;
        private readonly User _authenticatedUser;
        private readonly CategoryDAO _categories;
        private readonly RecipeDAO _recipes;
        private readonly ProductDAO _products;


        public RecipeController(
            IHttpContextAccessor httpContextAccessor,
            IWebHostEnvironment webHostEnvironment,
            ILogger<RecipeController> logger,
            MyRecipeDBContext dbContext
        )
        {
            _webHostEnvironment = webHostEnvironment;
            _logger = logger;
            _dbContext = dbContext;
            _categories = new CategoryDAO(dbContext);
            _recipes = new RecipeDAO(dbContext);
            _products = new ProductDAO(dbContext);

            String username = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            _authenticatedUser = _dbContext.Users.Where(user => user.Username == username).FirstOrDefault();
        }

        [HttpGet("List")]
        public IActionResult Index(int? categoryId, [FromQuery(Name = "productIds")] string productIdsStr, string search = null, int page = 1, int itemsPerPage = 12)
        {
            ViewData["search"] = search;
            ViewData["categoryId"] = categoryId;
            ViewData["productIds"] = productIdsStr;

            RecipesCategoriesDTO data = new RecipesCategoriesDTO();
            data.Categories = _categories.GetAll();
            data.Products = _products.GetAll();

            data.SelectedCategory = (categoryId != null)
                ? data.Categories.Where(category => category.Id == categoryId).First()
                : data.Categories.First();

            data.SelectedProductIds = JsonSerializer.Deserialize<List<int>>(productIdsStr ?? "[]");

            RecipesFilterDTO recipesFilter = new RecipesFilterDTO();
            recipesFilter.CategoryId = data.SelectedCategory.Name != "All"
                ? data.SelectedCategory.Id
                : null;
            recipesFilter.ProductIds = data.SelectedProductIds;
            recipesFilter.Name = search;

            data.Recipes = _recipes.GetFiltered(this._authenticatedUser, recipesFilter, page, itemsPerPage);
            return View(data);
        }

        [HttpGet("{recipeId}/Details")]
        public IActionResult RecipeDetails(int recipeId)
        {
            RecipeDetailsDTO recipeDetails = _recipes.GetDetails(this._authenticatedUser, recipeId);
            return View("RecipeDetails", recipeDetails);
        }


        [HttpGet("MyRecipes")]
        public IActionResult UserRecipes(UserRecipesDTO data, string search = null, int page = 1, int itemsPerPage = 12)
        {
            ViewData["search"] = search;
            data = new UserRecipesDTO();
            RecipesFilterDTO recipesFilter = new RecipesFilterDTO();
            recipesFilter.Name = search;
            recipesFilter.CreatedByUserId = this._authenticatedUser.Id;
            data.UserRecipes = _recipes.GetFiltered(this._authenticatedUser, recipesFilter, page, itemsPerPage);

            data.NewRecipe = new CreateRecipeDTO()
            {
                Categories = _categories.GetAll(false),
                Products = _products.GetAll(),
                MeasurementUnits = _products.GetMeasurementUnits()
            };

            return View("UserRecipes", data);
        }

        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public IActionResult CreateRecipe(CreateRecipeDTO newRecipe)
        {
            if (ModelState.IsValid)
            {
                newRecipe.Recipe.CreatedBy = this._authenticatedUser.Id;
                this._dbContext.Recipes.Add(newRecipe.Recipe);

                if (newRecipe.RecipeImages != null && newRecipe.RecipeImages.Count() > 0)
                {
                    bool primaryImage = true;
                    foreach (var imageFile in newRecipe.RecipeImages)
                    {
                        string uploadDir = Path.Combine(_webHostEnvironment.WebRootPath, "img/Recipe");
                        string fileName = Guid.NewGuid().ToString() + "_" + imageFile.FileName;
                        string filePath = Path.Combine(uploadDir, fileName);

                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            imageFile.CopyTo(fileStream);
                        }

                        RecipeImage recipeImage = new RecipeImage()
                        {
                            Recipe = newRecipe.Recipe,
                            ImagePath = fileName,
                            IsPrimary = primaryImage
                        };

                        this._dbContext.RecipeImages.Add(recipeImage);
                        primaryImage = false;
                    }
                }

                this._dbContext.SaveChanges();
            }

            return RedirectToAction("UserRecipes");
        }

        [HttpGet("Form/AddProduct")]
        public IActionResult CreateEditRecipeAddProduct(CreateRecipeDTO data)
        {
            data.Categories = _categories.GetAll(false);
            data.Products = _products.GetAll();
            data.MeasurementUnits = _products.GetMeasurementUnits();
            data.Recipe.Products.Add(new RecipeProduct());

            ModelState.Clear();

            return PartialView("_CreateEditRecipePartial", data);
        }

        [HttpGet("{recipeId}/Edit")]
        public IActionResult GetEditRecipeModal(int recipeId)
        {
            CreateRecipeDTO data = new CreateRecipeDTO()
            {
                Recipe = _recipes.GetById(recipeId, true),
                Categories = _categories.GetAll(false),
                Products = _products.GetAll(),
                MeasurementUnits = _products.GetMeasurementUnits()
            };

            if (data.Recipe.RecipeImages == null) {
                data.Recipe.RecipeImages = new List<RecipeImage>();
            }

            return PartialView("_CreateEditRecipePartial", data);
        }

        [HttpPost("{recipeId}/Edit")]
        [ValidateAntiForgeryToken]
        public IActionResult EditRecipe(CreateRecipeDTO newRecipeData)
        {
            Recipe recipe = _recipes.GetById(newRecipeData.Recipe.Id, true);
            if (recipe.CreatedBy != _authenticatedUser.Id)
            {
                Forbid();
            }

            recipe.Name = newRecipeData.Recipe.Name;
            recipe.CategoryId = newRecipeData.Recipe.CategoryId;
            recipe.Products = newRecipeData.Recipe.Products;
            recipe.TimeToCook = newRecipeData.Recipe.TimeToCook;
            recipe.Portions = newRecipeData.Recipe.Portions;
            recipe.Description = newRecipeData.Recipe.Description;


            if (newRecipeData.RecipeImages != null && newRecipeData.RecipeImages.Count() > 0)
            {
                foreach (RecipeImage recipeImage in recipe.RecipeImages)
                {
                    string uploadDir = Path.Combine(_webHostEnvironment.WebRootPath, "img/Recipe");
                    string filePath = Path.Combine(uploadDir, recipeImage.ImagePath);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                        this._dbContext.RecipeImages.Remove(recipeImage);
                    }
                }

                bool primaryImage = true;
                foreach (var imageFile in newRecipeData.RecipeImages)
                {
                    string uploadDir = Path.Combine(_webHostEnvironment.WebRootPath, "img/Recipe");
                    string fileName = Guid.NewGuid().ToString() + "_" + imageFile.FileName;
                    string filePath = Path.Combine(uploadDir, fileName);

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        imageFile.CopyTo(fileStream);
                    }

                    RecipeImage recipeImage = new RecipeImage()
                    {
                        RecipeId = recipe.Id,
                        ImagePath = fileName,
                        IsPrimary = primaryImage
                    };

                    recipe.RecipeImages.Add(recipeImage);
                    primaryImage = false;
                }
            }


            this._dbContext.Update(recipe);
            this._dbContext.SaveChanges();

            return RedirectToAction("RecipeDetails", new { recipeId = newRecipeData.Recipe.Id });
        }

        [HttpPost("{recipeId}/Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteRecipe(int recipeId)
        {
            Recipe recipe = _recipes.GetById(recipeId, true);
            if (recipe.CreatedBy != _authenticatedUser.Id)
            {
                Forbid();
            }


            if (recipe.RecipeImages != null && recipe.RecipeImages.Count() > 0)
            {
                foreach (RecipeImage recipeImage in recipe.RecipeImages)
                {
                    string uploadDir = Path.Combine(_webHostEnvironment.WebRootPath, "img/Recipe");
                    string filePath = Path.Combine(uploadDir, recipeImage.ImagePath);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                        this._dbContext.RecipeImages.Remove(recipeImage);
                    }
                }
            }

            this._dbContext.Remove(recipe);
            this._dbContext.SaveChanges();

            return RedirectToAction("UserRecipes");
        }

        [HttpGet("Favourites")]
        public IActionResult FavouriteRecipes(string search = null, int page = 1, int itemsPerPage = 12)
        {
            ViewData["search"] = search;
            RecipesFilterDTO recipesFilter = new RecipesFilterDTO();
            recipesFilter.FavouriteToUserId = this._authenticatedUser.Id;
            recipesFilter.Name = search;

            PaginatedList<RecipeDTO> favouriteRecipes = _recipes.GetFiltered(this._authenticatedUser, recipesFilter, page, itemsPerPage);
            return View("FavouriteRecipes", favouriteRecipes);
        }

        [HttpPost("{recipeId}/AddToFavourites")]
        public IActionResult AddToFavourites(int recipeId)
        {
            Recipe recipe = _recipes.GetById(recipeId);

            if (recipe == null)
            {
                return NotFound();
            }

            _dbContext.Add(new FavouriteRecipe
            {
                Recipe = recipe,
                User = _authenticatedUser
            });

            _dbContext.SaveChanges();
            return Ok();
        }

        [HttpPost("{recipeId}/RemoveFromFavourites")]
        public IActionResult RemoveFromFavourites(int recipeId)
        {
            FavouriteRecipe favRecipe = _recipes.GetUserFavouriteRecipe(_authenticatedUser.Id, recipeId);

            if (favRecipe == null)
            {
                return NotFound();
            }

            _dbContext.Remove(favRecipe);
            _dbContext.SaveChanges();
            return Ok();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route("Error")]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
