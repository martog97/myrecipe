﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyRecipe.Migrations
{
    public partial class FixFavRecipeToRecipeRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FavouriteRecipes_RecipeId",
                table: "FavouriteRecipes");

            migrationBuilder.CreateIndex(
                name: "IX_FavouriteRecipes_RecipeId",
                table: "FavouriteRecipes",
                column: "RecipeId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FavouriteRecipes_RecipeId",
                table: "FavouriteRecipes");

            migrationBuilder.CreateIndex(
                name: "IX_FavouriteRecipes_RecipeId",
                table: "FavouriteRecipes",
                column: "RecipeId");
        }
    }
}
