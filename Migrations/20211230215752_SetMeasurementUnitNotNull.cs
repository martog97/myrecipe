﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyRecipe.Migrations
{
    public partial class SetMeasurementUnitNotNull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RecipesProducts_MeasurementUnits_UnitId",
                table: "RecipesProducts");

            migrationBuilder.AlterColumn<int>(
                name: "UnitId",
                table: "RecipesProducts",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_RecipesProducts_MeasurementUnits_UnitId",
                table: "RecipesProducts",
                column: "UnitId",
                principalTable: "MeasurementUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RecipesProducts_MeasurementUnits_UnitId",
                table: "RecipesProducts");

            migrationBuilder.AlterColumn<int>(
                name: "UnitId",
                table: "RecipesProducts",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_RecipesProducts_MeasurementUnits_UnitId",
                table: "RecipesProducts",
                column: "UnitId",
                principalTable: "MeasurementUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
