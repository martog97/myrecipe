#pragma checksum "/home/martin/Projects/MyRecipe/Views/Auth/DeniedPage.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1a74013fd836d3124690a12cb17c1d746bffa621"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Auth_DeniedPage), @"mvc.1.0.view", @"/Views/Auth/DeniedPage.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/home/martin/Projects/MyRecipe/Views/_ViewImports.cshtml"
using MyRecipe;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/home/martin/Projects/MyRecipe/Views/_ViewImports.cshtml"
using MyRecipe.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1a74013fd836d3124690a12cb17c1d746bffa621", @"/Views/Auth/DeniedPage.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e3103289ba127d96dbcb3d4cd5866d1473dc5990", @"/Views/_ViewImports.cshtml")]
    public class Views_Auth_DeniedPage : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "/home/martin/Projects/MyRecipe/Views/Auth/DeniedPage.cshtml"
  
    Layout = null;
    ViewData["Title"] = "Permission Denied";

#line default
#line hidden
#nullable disable
            WriteLiteral("<h1>Permission Denied</h1>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
