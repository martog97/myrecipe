using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyRecipe.Models
{
    public class FavouriteRecipe
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int RecipeId { get; set; }

        [Required]
        public int UserId { get; set; }


        [ForeignKey("RecipeId")]
        public virtual Recipe Recipe { get; set; }

        public virtual User User { get; set; }
    }
}