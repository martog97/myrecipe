using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MyRecipe.Data.Enums;

namespace MyRecipe.Models
{
    public class Role
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public UserRoleEnum Name { get; set; } = UserRoleEnum.Client;

        

        [InverseProperty("Role")]
        public virtual List<User> Users { get; set;}
    }
}