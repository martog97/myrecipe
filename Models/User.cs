using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using MyRecipe.Data.Enums;

namespace MyRecipe.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        [MinLength(3, ErrorMessage = "Username should be at least 3 symbols.")]
        public string Username { get; set; }

        [Required]
        [MinLength(6, ErrorMessage = "Password should be at least 6 symbols.")]
        public string Password { get; set; }

        [Required]
        [EmailAddress]
        public string Email {get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "First name should be at least 2 symbols.")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "Last name should be at least 2 symbols.")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Birth date")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        [Required]
        [Display(Name = "Status")]
        public UserStatusEnum Status { get; set; } = UserStatusEnum.Active;

        [Required]
        public int RoleId { get; set; }


        [Required]
        [ValidateNever]
        public virtual Role Role { get; set; }

        [InverseProperty("CreatedByUser")]
        public virtual List<Recipe> Recipes { get; set; }

        public virtual List<FavouriteRecipe> FavouriteRecipes { get; set; }
    }
}