using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyRecipe.Models
{
    public class RecipeImage
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string ImagePath { get; set; }

        [Required]        
        public bool IsPrimary { get; set; }

        [Required]
        public int RecipeId { get; set; }


        public virtual Recipe Recipe { get; set; }
    }
}