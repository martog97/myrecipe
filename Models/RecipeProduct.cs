using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace MyRecipe.Models
{
    public class RecipeProduct
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int RecipeId { get; set; }

        [Required]
        public int ProductId { get; set; }

        [Required]
        [Range(1,9999)]
        public float Quantity {get; set; }

        [Required]
        public int UnitId { get; set; }

        
        public virtual Recipe Recipe { get; set; }

        public virtual Product Product { get; set; }
        
        public virtual MeasurementUnit Unit { get; set; }
    }
}