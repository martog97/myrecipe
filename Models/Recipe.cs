using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyRecipe.Models
{
    public class Recipe
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        public string Description { get; set; }
        
        [Required(ErrorMessage="The Time to cook field is required.")]
        public TimeSpan TimeToCook { get; set; }

        public int Portions { get; set; }


        [Required]
        public int CategoryId { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        [NotMapped]
        public bool IsFavourite { get; set; }


        public virtual Category Category { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual User CreatedByUser { get; set; }

        public virtual List<RecipeImage> RecipeImages { get; set; }

        public virtual FavouriteRecipe FavouriteRecipe { get; set; }
        
        [Required]
        public virtual List<RecipeProduct> Products { get; set; }
    }
}