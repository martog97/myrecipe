using System.ComponentModel.DataAnnotations;

namespace MyRecipe.Models
{
    public class MeasurementUnit
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        
        [Required]
        public string Short { get; set; }
    }
}