using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyRecipe.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(2)]
        public string Name { get; set; }
        
        public string Note { get; set; }
        

        public virtual List<RecipeProduct> Recipes { get; set; }
    }
}