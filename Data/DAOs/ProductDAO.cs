using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MyRecipe.Models;
using MyRecipe.Data.DTOs;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MyRecipe.Data.DAOs.Shared;
using System;

namespace MyRecipe.Data.DAOs
{
    public class ProductDAO
    {
        private MyRecipeDBContext _dbContext;
        public ProductDAO(MyRecipeDBContext dbContext)
        {
            _dbContext = dbContext;
        }


        public List<Product> GetAll()
        {
            return _dbContext.Products.ToList();
        }

        public PaginatedList<Product> GetFiltered(string searchString = null, int page = 1, int itemsPerPage = 15)
        {
            IQueryable<Product> productsQuery = _dbContext.Products.OrderByDescending(product => product.Id);

            if (!String.IsNullOrEmpty(searchString)) {
                productsQuery = productsQuery.Where(product => product.Name.Contains(searchString));
            }

            return PaginatedList<Product>.Create(productsQuery, page, itemsPerPage);
        }

        public Product GetById(int productId)
        {
            return _dbContext.Products.Where(product => product.Id == productId).FirstOrDefault();
        }

        public List<MeasurementUnit> GetMeasurementUnits()
        {
            return _dbContext.MeasurementUnits.ToList();
        }
    }
}