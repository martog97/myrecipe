using MyRecipe.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MyRecipe.Data.Enums;

namespace MyRecipe.Data.DAOs
{
    public class RoleDAO
    {
        private MyRecipeDBContext _dbContext;
        public RoleDAO(MyRecipeDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Role GetByName(UserRoleEnum name) {
            return _dbContext.Roles.Where(role => role.Name == name).FirstOrDefault();
        }
    }
}