using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MyRecipe.Models;
using MyRecipe.Data.DTOs;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MyRecipe.Data.DAOs.Shared;
using System;
using Microsoft.Data.SqlClient;
using System.Data;
using Npgsql;
using NpgsqlTypes;
using Newtonsoft.Json;

namespace MyRecipe.Data.DAOs
{
    public class RecipeDAO
    {
        private MyRecipeDBContext _dbContext;
        public RecipeDAO(MyRecipeDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Recipe GetById(int recipeId, bool includeAdditionalData = false)
        {
            if (includeAdditionalData == false) {
                return _dbContext.Recipes.Where((recipe) => recipe.Id == recipeId).FirstOrDefault();
            }

            return _dbContext.Recipes
                .Include(recipe => recipe.Products)
                .Include(recipe => recipe.Category)
                .Include(recipe => recipe.RecipeImages)
                .Where((recipe) => recipe.Id == recipeId)
                .FirstOrDefault();
        }

        public RecipeDetailsDTO GetDetails(User currentUser, int recipeId)
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(_dbContext.Database.GetConnectionString()))
            {
                string filteredRecipesQueryString = @"
                    select
                        r.""Id"",
                        r.""Name"",
                        r.""Description"",
                        jsonb_build_object(
                            'Id', c.""Id"",
                            'Name', c.""Name"",
                            'ImagePath', c.""ImagePath""
                        ) as ""Category"",
                        coalesce (
                            jsonb_agg(distinct
                                jsonb_build_object(
                                    'Id', p.""Id"",
                                    'Name', p.""Name"",
                                    'Note', p.""Note"",
                                    'Quantity', rp.""Quantity"",
                                    'Unit', mu.""Short""
                                )
                            ) filter (where rp.""Id"" notnull),
                            '[]'::jsonb
                        ) as ""Products"",
                        u.""Username"" as ""CreatedBy"",
                        coalesce (
                            jsonb_agg(distinct
                                jsonb_build_object(
                                    'Id', ri.""Id"",
                                    'ImagePath', ri.""ImagePath"",
                                    'IsPrimary', ri.""IsPrimary""
                                )
                            ) filter (where ri.""RecipeId"" notnull),
                            '[]'::jsonb
                        ) as ""Images"",
                        case when :currentUserId = any(array_agg(fr.""UserId""))
                            then true 
                            else false 
                        end as ""IsFavouriteToCurrentUser"",
                        r.""Portions"",
                        r.""TimeToCook""
                    from
                        ""Recipes"" r
                    join ""Categories"" c on
                        r.""CategoryId""  = c.""Id"" 
                    join ""Users"" u ON 
                        u.""Id"" = r.""CreatedBy""
                    left join ""RecipeImages"" ri on
                        ri.""RecipeId"" = r.""Id""
                    left join ""FavouriteRecipes"" fr
                        on fr.""RecipeId"" = r.""Id""
                    left join ""RecipesProducts"" rp on
                        rp.""RecipeId"" = r.""Id""
                    left join ""Products"" p on
                        p.""Id"" = rp.""ProductId""
                    left join  ""MeasurementUnits"" mu on
                        mu.""Id"" = rp.""UnitId""
                    where r.""Id"" = :recipeId
                    group by
                        r.""Id"",
                        c.""Id"",
                        u.""Id""
                ";

                NpgsqlCommand sqlCommand = new NpgsqlCommand(filteredRecipesQueryString, connection);
                sqlCommand.Parameters.AddWithValue("recipeId", NpgsqlDbType.Integer, recipeId);
                sqlCommand.Parameters.AddWithValue("currentUserId", NpgsqlDbType.Integer, currentUser.Id);

                RecipeDetailsDTO recipeDetails = new RecipeDetailsDTO();
                try
                {

                    connection.Open();
                    NpgsqlDataReader dataReader = sqlCommand.ExecuteReader();
                    while (dataReader.Read())
                    {
                        var values = new object[dataReader.FieldCount];
                        for (int i = 0; i < dataReader.FieldCount; i++)
                        {
                            values[i] = dataReader[i];
                        }

                        recipeDetails.Id = ConvertFromDBVal<int>(values[0]);
                        recipeDetails.Name = ConvertFromDBVal<string>(values[1]);
                        recipeDetails.Description = ConvertFromDBVal<string>(values[2]);
                        string categoryJson = ConvertFromDBVal<string>(values[3]);
                        recipeDetails.Category = JsonConvert.DeserializeObject<Category>(categoryJson);
                        string recipeProductsJson = ConvertFromDBVal<string>(values[4]);
                        recipeDetails.Products = JsonConvert.DeserializeObject<List<RecipeDetailsProductDTO>>(recipeProductsJson);
                        recipeDetails.CreatedBy = ConvertFromDBVal<string>(values[5]);
                        string recipeImagesJson = ConvertFromDBVal<string>(values[6]);
                        recipeDetails.Images = JsonConvert.DeserializeObject<List<RecipeImage>>(recipeImagesJson);
                        recipeDetails.FavouriteToCurrentUser = ConvertFromDBVal<bool>(values[7]);
                        recipeDetails.Portions = ConvertFromDBVal<int>(values[8]);

                        TimeSpan timeToCook = ConvertFromDBVal<TimeSpan>(values[9]);

                        int timeToCookMinutes =  timeToCook.Minutes;

                        if (timeToCook.Hours > 0) {
                            timeToCookMinutes += timeToCook.Hours * 60;
                        }

                        recipeDetails.TimeToCook = timeToCookMinutes + " min";
                    }

                    
                    dataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    connection.Close();
                }

                return recipeDetails;
            }
        }

        public PaginatedList<RecipeDTO> GetFiltered(User currentUser, RecipesFilterDTO filter, int page = 1, int itemsPerPage = 12)
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(_dbContext.Database.GetConnectionString()))
            {
                string filteredRecipesQueryString = @"
                    with ""recipeProducts"" as (
                        select
                            r.""Id"",
                            r.""Name"",
                            r.""Description"",
                            r.""CategoryId"",
                            r.""CreatedBy"",
                            r.""Portions"",
                            r.""TimeToCook"",
                            array_agg(p.""Id"") as ""ProductIds""
                        from 
                            ""Recipes"" r 
                        left join ""RecipesProducts"" rp
                            on rp.""RecipeId""  = r.""Id""
                        left join ""Products"" p
                            on rp.""ProductId""  = p.""Id""
                        group by
                            r.""Id""
                    ),
                    ""filteredRecipes"" as (
                        select
                            rp.""Id"",
                            rp.""Name"",
                            rp.""Description"",
                            rp.""CategoryId"",
                            rp.""Portions"",
                            rp.""TimeToCook"",
                            u.""Username"" as ""CreatedBy"",
                            MAX(ri.""ImagePath"") as ""ImagePath"",
                            case when :currentUserId::int = any(array_agg(fr.""UserId"")) then
                                true
                            else
                                false
                            end as ""FavouriteToCurrentUser""
                        from 
                            ""recipeProducts"" rp
                        left join ""RecipeImages"" ri
                            on ri.""RecipeId"" = rp.""Id"" and ri.""IsPrimary"" = true
                        join ""Users"" u
                            on u.""Id""  = rp.""CreatedBy""
                        left join ""FavouriteRecipes"" as fr
                            on fr.""RecipeId"" = rp.""Id""
                        where 
                            rp.""ProductIds"" @> :productIds::int[]
                            and (
                                :recipeName isnull
                                or rp.""Name"" ilike '%' || :recipeName || '%'
                            )
                            and (
                                :categoryId isnull
                                or :categoryId = rp.""CategoryId""
                            )
                            and (
                                :createdByUserId isnull
                                or :createdByUserId = u.""Id""
                            )
                            and (
                                :favouriteToUserId isnull
                                or :favouriteToUserId = fr.""UserId""
                            )
                        group by
                            rp.""Id"",
                            rp.""Name"",
                            rp.""Id"",
                            rp.""Name"",
                            rp.""Description"",
                            rp.""CategoryId"",
                            rp.""Portions"",
                            rp.""TimeToCook"",
                            u.""Username""
                    ),
                    ""recipesCount"" as (
                        select
                            count(rp.""Id"") as cnt
                        from
                            ""filteredRecipes"" rp
                    ),
                    ""recipesPaginated"" as (
                        select
                            rp.*
                        from
                            ""filteredRecipes"" rp
                        limit 
                            :itemsPerPage
                        offset
                            :offsetItems
                    )
                    select
                        json_agg(rp.*) as recipes,
                        rc.cnt as total
                    from
                        ""recipesPaginated"" rp,
                        ""recipesCount"" rc
                    group by
                        rc.cnt
                ";

                NpgsqlCommand sqlCommand = new NpgsqlCommand(filteredRecipesQueryString, connection);

                if (filter.Name != null && filter.Name.Length > 0)
                {
                    sqlCommand.Parameters.AddWithValue("recipeName", NpgsqlDbType.Varchar, filter.Name);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("recipeName", NpgsqlDbType.Bigint, DBNull.Value);
                }

                if (filter.CategoryId != null)
                {
                    sqlCommand.Parameters.AddWithValue("categoryId", NpgsqlDbType.Integer, (int)filter.CategoryId);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("categoryId", NpgsqlDbType.Bigint, DBNull.Value);
                }

                if (filter.CreatedByUserId != null)
                {
                    sqlCommand.Parameters.AddWithValue("createdByUserId", NpgsqlDbType.Integer, filter.CreatedByUserId);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("createdByUserId", NpgsqlDbType.Integer, DBNull.Value);
                }

                if (filter.FavouriteToUserId != null)
                {
                    sqlCommand.Parameters.AddWithValue("favouriteToUserId", NpgsqlDbType.Integer, filter.FavouriteToUserId);
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue("favouriteToUserId", NpgsqlDbType.Integer, DBNull.Value);
                }


                sqlCommand.Parameters.AddWithValue("currentUserId", NpgsqlDbType.Integer, currentUser.Id);
                sqlCommand.Parameters.AddWithValue("productIds", NpgsqlDbType.Array | NpgsqlDbType.Integer, filter.ProductIds ?? new List<int>());
                sqlCommand.Parameters.AddWithValue("itemsPerPage", NpgsqlDbType.Integer, itemsPerPage);
                sqlCommand.Parameters.AddWithValue("offsetItems", NpgsqlDbType.Integer, (page - 1) * itemsPerPage);

                List<RecipeDTO> filteredRecipes = new List<RecipeDTO>();
                int filteredRecipesCount = 0;

                try
                {

                    connection.Open();
                    NpgsqlDataReader dataReader = sqlCommand.ExecuteReader();
                    while (dataReader.Read())
                    {
                        var values = new object[dataReader.FieldCount];
                        for (int i = 0; i < dataReader.FieldCount; i++)
                        {
                            values[i] = dataReader[i];
                        }

                        string filteredRecipesJson = ConvertFromDBVal<string>(values[0]);
                        filteredRecipes = JsonConvert.DeserializeObject<List<RecipeDTO>>(filteredRecipesJson);
                        filteredRecipesCount = (int)ConvertFromDBVal<Int64>(values[1]);
                    }
                    dataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    connection.Close();
                }

                return new PaginatedList<RecipeDTO>(filteredRecipes, filteredRecipesCount, page, itemsPerPage);
            }
        }

        public FavouriteRecipe GetUserFavouriteRecipe(int userId, int recipeId)
        {
            return _dbContext.FavouriteRecipes
                .Where((recipe) => recipe.RecipeId == recipeId)
                .Where((recipe) => recipe.UserId == userId)
                .FirstOrDefault();
        }

        private static T ConvertFromDBVal<T>(object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return default(T); // returns the default value for the type
            }
            else
            {
                return (T)obj;
            }
        }
    }
}