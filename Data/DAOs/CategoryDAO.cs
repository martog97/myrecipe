using System.Collections.Generic;
using MyRecipe.Models;
using System.Linq;
using MyRecipe.Data.DAOs.Shared;
using System;

namespace MyRecipe.Data.DAOs
{
    public class CategoryDAO
    {
        private MyRecipeDBContext _dbContext;
        public CategoryDAO(MyRecipeDBContext dbContext)
        {
            _dbContext = dbContext;
        }
        public List<Category> GetAll(bool includeAllCategory = true)
        {
            if (includeAllCategory) {
                return _dbContext.Categories.OrderBy(category => category.Name).ToList();
            }

            return _dbContext.Categories.Where((category) => category.Name != "All").OrderBy(category => category.Name).ToList();
        }

        public Category GetById(int categoryId)
        {
            return _dbContext.Categories.Where((category) => category.Id == categoryId).FirstOrDefault();
        }

        public PaginatedList<Category> GetFiltered(string searchString = null, int page = 1, int itemsPerPage = 15)
        {
            IQueryable<Category> categoriesQuery = _dbContext.Categories.OrderBy(category => category.Name);

            if (!String.IsNullOrEmpty(searchString)) {
                categoriesQuery = categoriesQuery.Where(category => category.Name.Contains(searchString));
            }

            return PaginatedList<Category>.Create(categoriesQuery, page, itemsPerPage);
        }
    }
}