using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MyRecipe.Data.DAOs.Shared
{
    public class PaginatedList<T> : List<T>
    {
        public int PageIndex { get; private set; }
        public int TotalPages { get; private set; }

        public PaginatedList(List<T> items, int count, int pageIndex, int itemsPerPage)
        {
            PageIndex = pageIndex;
            TotalPages = (int) Math.Ceiling(count / (double) itemsPerPage);

            this.AddRange(items);
        }

        public bool HasPreviousPage
        {
            get
            {
                return (PageIndex > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageIndex < TotalPages);
            }
        }

        public static PaginatedList<T> Create(IQueryable<T> source, int pageIndex, int itemsPerPage)
        {
            var count = source.Count();
            var items = source.Skip((pageIndex - 1) * itemsPerPage).Take(itemsPerPage).ToList();
            return new PaginatedList<T>(items, count, pageIndex, itemsPerPage);
        }
    }
}