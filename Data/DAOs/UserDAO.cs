using MyRecipe.Models;
using System.Linq;
using MyRecipe.Data.DAOs.Shared;
using System;
using Microsoft.EntityFrameworkCore;

namespace MyRecipe.Data.DAOs
{
    public class UserDAO
    {
        private MyRecipeDBContext _dbContext;
        public UserDAO(MyRecipeDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public PaginatedList<User> GetFiltered(string searchString = null, int page = 1, int itemsPerPage = 15, User currentUser = null)
        {
            IQueryable<User> usersQuery = _dbContext.Users.Include(user => user.Role).OrderByDescending(user => user.Username);

            if (!String.IsNullOrEmpty(searchString))
            {
                usersQuery = usersQuery.Where(user => user.Username.Contains(searchString) || user.Email.Contains(searchString));
            }

            if (currentUser != null) {
                usersQuery = usersQuery.Where(user => user.Id != currentUser.Id);
            }

            return PaginatedList<User>.Create(usersQuery, page, itemsPerPage);
        }

        public User GetByUsername(string username) {
            return _dbContext.Users.Include(user => user.Role).Where(user => user.Username == username).FirstOrDefault();
        }
    }
}