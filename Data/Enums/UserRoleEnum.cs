namespace MyRecipe.Data.Enums
{
    public enum UserRoleEnum
    {
        Client,
        Admin
    }
}