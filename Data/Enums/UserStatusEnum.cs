namespace MyRecipe.Data.Enums
{
    public enum UserStatusEnum
    {
        Active,
        Inactive
    }
}