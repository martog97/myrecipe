using System;
using Microsoft.EntityFrameworkCore;
using MyRecipe.Data.Enums;
using MyRecipe.Models;

#nullable disable

namespace MyRecipe.Data
{
    public partial class MyRecipeDBContext : DbContext
    {

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<RecipeProduct> RecipesProducts { get; set; }
        public DbSet<RecipeImage> RecipeImages { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<FavouriteRecipe> FavouriteRecipes { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<MeasurementUnit> MeasurementUnits { get; set; }

        public MyRecipeDBContext(DbContextOptions<MyRecipeDBContext> options)
            : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<User>()
                .Property(user => user.Status)
                .HasConversion(
                    status => status.ToString(),
                    status => (UserStatusEnum)Enum.Parse(typeof(UserStatusEnum), status)
                );

            builder.Entity<Role>()
                .Property(role => role.Name)
                .HasConversion(
                    roleName => roleName.ToString(),
                    roleName => (UserRoleEnum)Enum.Parse(typeof(UserRoleEnum), roleName)
                );
        }
    }
    
}