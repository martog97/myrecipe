using Microsoft.AspNetCore.Http;
using System;

namespace MyRecipe.Data.DTOs
{
    public class CreateEditCategoryDTO
    {
        public int? Id { get; set; }
        public String Name { get; set; }
        public String ImagePath { get; set; }
        public IFormFile Image { get; set; }
    }
}