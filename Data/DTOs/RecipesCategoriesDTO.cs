using System.Collections.Generic;
using MyRecipe.Data.DAOs.Shared;
using MyRecipe.Models;

namespace MyRecipe.Data.DTOs
{
    public class RecipesCategoriesDTO
    {
        public List<Category> Categories { get; set; }
        public Category SelectedCategory { get; set; }

        public List<Product> Products { get; set; }
        public List<int> SelectedProductIds { get; set; }

        public PaginatedList<RecipeDTO> Recipes { get; set; }

        public int Page { get; set; }

        public int ItemsPerPage { get; set; }


    }
}