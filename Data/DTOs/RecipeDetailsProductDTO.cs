namespace MyRecipe.Data.DTOs
{
    public class RecipeDetailsProductDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public int Quantity { get; set; }
        public string Unit { get; set; }

    }
}