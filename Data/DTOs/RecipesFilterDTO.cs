using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MyRecipe.Models;

namespace MyRecipe.Data.DTOs
{
    public class RecipesFilterDTO
    {
        #nullable enable
        public string? Name { get; set; }

        public int? CategoryId { get; set; }
        public int? CreatedByUserId { get; set; }
        public int? FavouriteToUserId { get; set; }

        public List<int>? ProductIds { get; set; }
        #nullable disable
    }
}