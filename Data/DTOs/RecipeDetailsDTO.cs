using System.Collections.Generic;
using MyRecipe.Models;

namespace MyRecipe.Data.DTOs
{
    public class RecipeDetailsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int Portions { get; set; }

        public string TimeToCook { get; set; }

        public Category Category { get; set; }
        public List<RecipeDetailsProductDTO> Products { get; set; }
        public string CreatedBy { get; set; }
        public List<RecipeImage> Images {get; set; }
        public bool FavouriteToCurrentUser {get; set; }

    }
}