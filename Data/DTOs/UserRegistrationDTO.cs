using System.ComponentModel.DataAnnotations;
using MyRecipe.Models;

namespace MyRecipe.Data.DTOs
{
    public class UserRegistrationDTO : User
    {
       [Required]
       [Display(Name = "Confirm password")]
       [Compare("Password", ErrorMessage = "The passwords do not match.")]
        public string ConfirmPassword { get; set; }
    }
}