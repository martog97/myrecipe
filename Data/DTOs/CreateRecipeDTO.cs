using System.Collections.Generic;
using MyRecipe.Models;
using Microsoft.AspNetCore.Http;
using System;

namespace MyRecipe.Data.DTOs
{
    public class CreateRecipeDTO
    {
        public Recipe Recipe { get; set; }
        public List<Category> Categories { get; set; }
        public List<Product> Products { get; set; }
        public List<MeasurementUnit> MeasurementUnits { get; set; }
        public List<IFormFile> RecipeImages { get; set; }

        public CreateRecipeDTO()
        {
            this.Recipe = new Recipe() {
                Products = new List<RecipeProduct>() {
                    new RecipeProduct() {},
                }
            };
            this.Recipe.Portions = 1;
            this.Recipe.TimeToCook = new TimeSpan(0, 10, 0);
            this.Recipe.RecipeImages = new List<RecipeImage>();
            this.initCollections();

        }

        public CreateRecipeDTO(Recipe recipe)
        {
            this.Recipe = recipe;
            this.initCollections();
        }

        private void initCollections() {
            this.Categories = new List<Category>();
            this.Products = new List<Product>();
            this.MeasurementUnits = new List<MeasurementUnit>();
            this.RecipeImages = new List<IFormFile>();
        }
    }
}