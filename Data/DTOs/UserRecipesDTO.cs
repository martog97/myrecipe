using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MyRecipe.Data.DAOs.Shared;
using MyRecipe.Models;

namespace MyRecipe.Data.DTOs
{
    public class UserRecipesDTO
    {
        public PaginatedList<RecipeDTO> UserRecipes { get; set; }
        public CreateRecipeDTO NewRecipe { get; set; }
    }
}