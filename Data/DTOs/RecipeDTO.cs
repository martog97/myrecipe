using System;

namespace MyRecipe.Data.DTOs
{
    public class RecipeDTO
    {
        #nullable enable
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int CategoryId { get; set; }
        public string? CreatedBy { get; set; }
        public string? ImagePath { get; set; }
        public int Portions { get; set; }
        public TimeSpan TimeToCook { get; set; }
        public bool FavouriteToCurrentUser { get; set; }
        #nullable disable

    }
}