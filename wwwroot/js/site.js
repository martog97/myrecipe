﻿/* Recipes page */
function FilterRecipesByCategory(categoryId) {
    const queryParams = getQueryParams();
    categoryId = parseInt(categoryId);
    queryParams["categoryId"] = categoryId;
    queryParams["page"] = 1;

    const paramsStr = decodeURIComponent($.param(queryParams));
    let redirectUrl = `/Recipe/List?${paramsStr}`;

    window.location.assign(redirectUrl);
}

function FilterRecipesByProduct(productId) {
    const queryParams = getQueryParams();
    productId = parseInt(productId);

    let queryProductIds = JSON.parse(queryParams["productIds"] ? queryParams["productIds"] : '[]');
    if (queryProductIds.includes(productId)) {
        queryProductIds = queryProductIds.filter(id => id !== productId);
    } else {
        queryProductIds.push(productId);
    }

    if (queryParams["productIds"] && queryProductIds.length === 0) {
        delete queryParams["productIds"];
    } else {
        queryParams["productIds"] = JSON.stringify(queryProductIds);
    }

    queryParams["page"] = 1;

    const paramsStr = decodeURIComponent($.param(queryParams));
    let redirectUrl = `/Recipe/List?${paramsStr}`;

    window.location.assign(redirectUrl);
}

function ShowRecipeDetails(recipeId) {
    window.location.assign(`${recipeId}/Details`);
}

function ShowCreateRecipeModal() {
    $('#createRecipeModal').appendTo("body").modal('show');
}

function HideCreateRecipeModal() {
    $('#createRecipeModal').modal('hide');
    $('#createEditRecipeForm')[0].reset();
}

function ShowEditRecipeModal(recipeId) {
    const url = `/Recipe/${recipeId}/Edit`;

    $.ajax({
        type: 'GET',
        url,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function(result) {
            $("#createEditRecipeForm").replaceWith(result);
            $('#editRecipeModal').appendTo("body").modal('show');
        },
        error: function() {
            alert('Failed show modal.');
        }
    });
}

function HideEditRecipeModal() {
    $('#editRecipeModal').modal('hide');
    $('#createEditRecipeForm')[0].reset();
}

function SubmitRecipeForm() {
    const form = $("#createEditRecipeForm");

    if (!form[0].checkValidity()) {
        form[0].reportValidity();
        return;
    }

    form.submit();
    HideCreateRecipeModal();
}

function CreateEditRecipeFormAddProductField() {
    const data = $("#createEditRecipeForm").serialize();

    $.ajax({
        type: 'GET',
        url: '/Recipe/Form/AddProduct',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        data: data,
        success: function(result) {
            $('#createEditRecipeForm').replaceWith(result);
        },
        error: function() {
            alert('Failed to add Product');
        }
    });
}

function CreateRecipeRemoveProductField(productNumber) {
    $(`#recipeProduct${productNumber}`).remove();
}

function AddRecipeToFavourites(recipeId) {
    $.ajax({
        type: 'POST',
        url: `/Recipe/${recipeId}/AddToFavourites`,
        success: function() {
            location.reload();
        },
        error: function() {
            alert('Error adding recipe to favourites!');
        }
    });
}

function RemoveRecipeFromFavourites(recipeId) {
    $.ajax({
        type: 'POST',
        url: `/Recipe/${recipeId}/RemoveFromFavourites`,
        success: function() {
            location.reload();
        },
        error: function() {
            alert('Error removing recipe from favourites!');
        }
    });
}

function updateCreateEditRecipeImagesPreview(event) {
    const upladedFiles = Array.from(event.target.files);

    let newImagesHtml = "";
    upladedFiles.forEach(file => {
        const recipeImageWrapper = document.createElement("div");
        recipeImageWrapper.className = "col-auto mx-1 mt-08 d-flex";

        const img = document.createElement("img");
        img.className = "create-edit-recipe-image border rounded p-2";
        img.setAttribute('src', URL.createObjectURL(file));

        recipeImageWrapper.append(img)
        newImagesHtml += recipeImageWrapper.outerHTML;
    });

    $('#recipeImages').get(0).innerHTML = newImagesHtml;
}

/* Pagination */
function RedirectToPage(page, clearParams = false) {
    let url = window.location.pathname;

    if (clearParams) {
        window.location.assign(url);
        return;
    }

    let queryParams = getQueryParams();
    queryParams.page = page;

    const queryString = objectToQueryString(queryParams);
    url += queryString

    window.location.assign(url);
}

function getQueryParams() {
    let queryParams = {};

    if (window.location.search.length === 0) {
        return queryParams;
    }

    const query = window.location.search.substring(1);
    const pairs = query.split("&");

    pairs.forEach((keyValueStr) => {
        const keyValueArr = keyValueStr.split("=");
        queryParams[keyValueArr[0]] = decodeURIComponent(keyValueArr[1]);
    })

    return queryParams;
}

function objectToQueryString(obj) {
    let queryString = "?";

    Object.entries(obj).forEach(([key, value], index, array) => {
        queryString += `${key}=${value}`;
        if (index !== (array.length - 1)) {
            queryString += "&";
        }

    })

    return queryString;
}

/* Products page */

/* Create product modal */
function ShowCreateProductModal() {
    $('#createProductModal').appendTo("body").modal('show');
}

function HideCreateProductModal() {
    $('#createProductModal').modal('hide');
    $('#createProductForm')[0].reset();
}

function SubmitCreateProductForm() {
    const form = $("#createProductForm");

    if (!form[0].checkValidity()) {
        form[0].reportValidity();
        return;
    }

    form.submit();
    HideCreateProductModal();
}


/* Edit product modal */
function ShowEditProductModal(productId) {
    const url = `/Product/${productId}/Edit`;

    $.ajax({
        type: 'GET',
        url,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function(result) {
            $("#editProductModal").replaceWith(result);
            $('#editProductModal').appendTo("body").modal('show');
        },
        error: function() {
            alert('Failed show modal.');
        }
    });
}

function HideEditProductModal() {
    $('#editProductModal').modal('hide');
    $('#editProductForm')[0].reset();
}

function SubmitEditProductForm() {
    const form = $("#editProductForm");

    if (!form[0].checkValidity()) {
        form[0].reportValidity();
        return;
    }

    form.submit();
    HideEditProductModal();
}

/* Categories Page */

/* Create category modal */
function ShowCreateCategoryModal() {
    $('#createCategoryModal').appendTo("body").modal('show');
}

function HideCreateCategoryModal() {
    $('#createCategoryModal').modal('hide');
    $('#createCategoryForm')[0].reset();
}

function SubmitCreateCategoryForm() {
    const form = $("#createCategoryForm");

    if (!form[0].checkValidity()) {
        form[0].reportValidity();
        return;
    }

    form.submit();
    HideCreateCategoryModal();
}

/* Edit category modal */
function ShowEditCategoryModal(categoryId) {
    const url = `/Category/${categoryId}/Edit`;

    $.ajax({
        type: 'GET',
        url,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function(result) {
            $("#editCategoryModal").replaceWith(result);
            $('#editCategoryModal').appendTo("body").modal('show');
        },
        error: function() {
            alert('Failed show modal.');
        }
    });
}

function HideEditCategoryModal() {
    $('#editCategoryModal').modal('hide');
    $('#editCategoryForm')[0].reset();
}

function SubmitEditCategoryForm() {
    const form = $("#editCategoryForm");

    if (!form[0].checkValidity()) {
        form[0].reportValidity();
        return;
    }

    form.submit();
    HideEditCategoryModal();
}

function updateCreateEditCategoryImagePreview(event, imageElementId, defaultValue) {
    const [file] = event.target.files;

    if (file) {
        $(`#${imageElementId}`).attr("src", URL.createObjectURL(file))
        $(`#${imageElementId}`).parent().addClass("d-flex justify-content-center");
        $(`#${imageElementId}`).parent().removeClass("d-none");
    } else {
        $(`#${imageElementId}`).attr("src", defaultValue);
    }
}

/* Users Page */

/* Edit user modal */
function ShowEditUserModal(username) {
    const url = `/User/${username}/Edit`;

    $.ajax({
        type: 'GET',
        url,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function(result) {
            $("#editUserModal").replaceWith(result);
            $('#editUserModal').appendTo("body").modal('show');
        },
        error: function() {
            alert('Failed show modal.');
        }
    });
}

function HideEditUserModal() {
    $('#editUserModal').modal('hide');
    $('#editUserForm')[0].reset();
}

function SubmitEditUserForm() {
    const form = $("#editUserForm");

    if (!form[0].checkValidity()) {
        form[0].reportValidity();
        return;
    }

    form.submit();
    HideEditUserModal();
}


/* Delete confirmation modal */
function ShowDeleteConfirmationModal(currentElement, modalTitle, modalText) {
    let parentForm = currentElement.parentNode;
    $('#modalTitle').html(modalTitle);
    $('#modalText').html(modalText);

    $('#deleteConfirmationModal').appendTo("body").modal('show');
    $('#confirmDeleteBtn').click(() => {
        parentForm.submit();
        HideDeleteConfirmationModal();
    })
}

function HideDeleteConfirmationModal() {
    $('#deleteConfirmationModal').appendTo("body").modal('hide');
}